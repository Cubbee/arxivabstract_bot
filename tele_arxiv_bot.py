import json
import re
import urllib.request as request
import xml.etree.ElementTree as ET
import telepot
from telepot.loop import MessageLoop
import sys
import time
import random

def maybe_get_arxiv_paper_id(string):
    candidate_paper_links = re.findall(r'(https?://\S+)', string)
    paper_ids = []
    paper_links = []
    for paper_link in candidate_paper_links:
        paper_id = re.search('\d*\.\d*$', paper_link)
        if paper_id:
            paper_id = paper_id.group(0)
        else:
            paper_id_candidate = re.search('(\d+\.\d+.*)\.pdf', paper_link)
            if paper_id_candidate:
                paper_id = paper_id_candidate.group(1)
        paper_ids.append(paper_id)
        paper_links.append(paper_link)
    return paper_ids, paper_links

def get_paper_description(paper_id):
    url = 'http://export.arxiv.org/api/query?id_list=' + paper_id
    response = request.urlopen(url).read()
    parsed = ET.fromstring(response)
    entry = parsed.find('{http://www.w3.org/2005/Atom}entry')
    title = entry.find('{http://www.w3.org/2005/Atom}title').text
    summary = entry.find('{http://www.w3.org/2005/Atom}summary').text
    return title, summary


if __name__ == '__main__':
    def handle(msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        print(content_type, chat_type, chat_id)

        if msg['from']['id'] != bot.getMe()['id'] and content_type == 'text':
            paper_ids, paper_links = maybe_get_arxiv_paper_id(msg['text'])
            for paper_id, paper_link in zip(paper_ids, paper_links):
                title, summary = get_paper_description(paper_id)
                bot.sendMessage(chat_id, '{}\n*{}*\n{}'.format(paper_link, title.replace('\n', ' '), summary.replace('\n', ' ')), parse_mode='MARKDOWN')


    from telepot.namedtuple import InlineQueryResultArticle, InputTextMessageContent

    '''
    def on_inline_query(msg):
        def compute():
            query_id, from_id, query_string = telepot.glance(msg, flavor='inline_query')
            print('Inline Query:', query_id, from_id, query_string)

            paper_id, paper_link = maybe_get_arxiv_paper_id(query_string)
            ans = ''
            if paper_id:
                title, summary = get_paper_description(paper_id)
                ans = '{}\n*{}*\n{}'.format(paper_link, title.replace('\n', ' '), summary.replace('\n', ' '))

            articles = [InlineQueryResultArticle(
                            id='abc',
                            title=query_string,
                            input_message_content=InputTextMessageContent(
                                message_text=ans,
                                parse_mode='MARKDOWN'
                            )
                    )]

            return articles

        answerer.answer(msg, compute)

    def on_chosen_inline_result(msg):
        result_id, from_id, query_string = telepot.glance(msg, flavor='chosen_inline_result')
        print ('Chosen Inline Result:', result_id, from_id, query_string)
    '''

    def on_inline_query(msg):
        def compute():
            query_id, from_id, query_string = telepot.glance(msg, flavor='inline_query')
            print('Inline Query:', query_id, from_id, query_string)
            print(query_string)
            paper_ids, paper_links = maybe_get_arxiv_paper_id(query_string)
            anss = []
            titles = []
            for paper_id, paper_link in zip(paper_ids, paper_links):
                title, summary = get_paper_description(paper_id)
                ans = '{}\n*{}*\n{}'.format(paper_link, title.replace('\n', ' '), summary.replace('\n', ' '))
                anss.append(ans)
                titles.append(title)

            print(paper_ids, paper_links, titles)

            if not anss:
                return

            articles = [InlineQueryResultArticle(
                            id='abc' + paper_link + str(random.random()),
                            title=paper_title,
                            input_message_content=InputTextMessageContent(
                                message_text=ans,
                                parse_mode='Markdown'
                            )
                    ) for paper_id, paper_link, paper_title, ans in zip(paper_ids, paper_links, titles, anss)]

            return articles

        answerer.answer(msg, compute)

    def on_chosen_inline_result(msg):
        result_id, from_id, query_string = telepot.glance(msg, flavor='chosen_inline_result')
        print ('Chosen Inline Result:', result_id, from_id, query_string)




    TOKEN = sys.argv[1]  # get token from command-line

    bot = telepot.Bot(TOKEN)
    # MessageLoop(bot, handle).run_as_thread()
    answerer = telepot.helper.Answerer(bot)

    MessageLoop(bot, {'chat': handle, 'inline_query': on_inline_query,
                    'chosen_inline_result': on_chosen_inline_result}).run_as_thread()
    print ('Listening ...')

    # Keep the program running.
    while 1:
        time.sleep(10)
